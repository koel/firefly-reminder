# firefly-reminder

This is a simple script for reminding you to categorise transactions in firefly. Specifically it:
- Checks the last 7 days of transactions for any that are not categorised.
- If there are any, it creates/updates a task in Todoist.

## Installation

```sh
pip install -r requirements.txt

# configure
cp example.env .env
vim .env
```

## Running

Recommended to run this on a cron. Run as frequently as you like. 

```sh
python3 firefly-reminder.py
```

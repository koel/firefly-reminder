#!/usr/bin/env python3

from dotenv import load_dotenv
import os
from datetime import date, timedelta
from todoist.api import TodoistAPI
import re
import requests

DATE_FORMAT = '%Y-%m-%d'

load_dotenv()  # take environment variables from .env.

if __name__ == '__main__':
    ff_api_base = f"{os.getenv('FF_HOST')}/api/v1"
    ff_auth = f'Bearer {os.getenv("FF_ACCESS_TOKEN")}'

    cats_missing = 0

    trans_resp = requests.get(ff_api_base+'/transactions', params={
        'page': '1',
        'start': (date.today() - timedelta(days=7)).strftime(DATE_FORMAT),
        'end': date.today().strftime(DATE_FORMAT),
        'type': 'expense'
    }, headers={
        'Accept': 'application/json',
        'Authorization': ff_auth
    })

    if not trans_resp.status_code == 200:
        print('Failed to get transactions from firefly. Response code:', trans_resp.status_code)
        exit(1)

    for trans_grp in trans_resp.json()['data']:
        for trans in trans_grp['attributes']['transactions']:
            if trans['category_name'] is None:
                cats_missing += 1

    if cats_missing > 0:
        # todoist
        todo_api = TodoistAPI(token=os.getenv('TODOIST_TOKEN'))
        todo_api.sync()
        # get @auto tasks
        auto_tasks = todo_api.items.all(lambda task: task.data['checked'] == 0 and 2160695681 in task.data['labels'])

        task_already_set = False
        today = date.today().strftime('%Y-%m-%d')
        for task in auto_tasks:
            match = re.match('^Categorise ([0-9]+) transactions?$', task.data['content'])
            if match is not None:
                task_already_set = True
                due = task.data['due']['date'][:10]  # just date part
                if due != today:
                    # move due date to today
                    task.update(due={'date': today})

                task_count = int(match.groups()[0])
                if task_count != cats_missing:
                    # set correct task count
                    task.update(content=f'Categorise {cats_missing} transaction'
                                        f'{"" if cats_missing == 1 else "s"}')

                print('Task updated')
                break

        if not task_already_set:
            todo_api.items.add(f'Categorise {cats_missing} transaction{"" if cats_missing == 1 else "s"}',
                               labels=[2160695681,], due={'date': today})
            print('New task created')

        todo_api.commit()
        print('Todoist sync complete')

    else:
        print('All transactions in the last week are already categorised')
